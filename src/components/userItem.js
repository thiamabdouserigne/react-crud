import React from "react";
import { deleteUser, getUsersInfo } from "../services/LocalStorage";


export const UserItem = ({user,setUsers}) => {
    const {id,prenom,nom,email,phone} = user;
    const removingUser = () => {
        deleteUser(id);
        setUsers(getUsersInfo())
    }
    
    return (
       <tr>
       <th>{prenom}</th>
       <th>{nom}</th>
       <th>{email}</th>
       <th>{phone}</th>
       <th>
        <div>
            <span role='button' className='badge bg-success m-2' >Modifier</span>
            <span role='button' className='badge bg-danger m-2' onClick={() => removingUser()} >Supprimer</span>
        </div>
       </th>
        
       </tr>
    )
}