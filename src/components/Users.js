import React, { useEffect, useState } from "react";
import { UserItem } from "./userItem";
import { getUsersInfo } from "../services/LocalStorage";

function Users(){
    const [users,setUsers] = useState([])

    useEffect(()=>{
        setUsers(getUsersInfo())
    },[])
    return(
        <div>
        <h2>Utilisateurs</h2>
        
                <table>
                    <thead>
                        <tr>
                            <th>Prenom</th>
                            <th>Nom</th>
                            <th>Email</th>
                            <th>Telephone</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    {users.map(user =>  <UserItem user={user} key={user.id} setUser={setUsers}/>)}
                       
                    </tbody>
                </table>
        </div>
        
    )
}

export default Users