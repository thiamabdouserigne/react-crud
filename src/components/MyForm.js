import React from "react";
import { addUsers } from "../services/LocalStorage";
import '../style/form.css'
import { UserForm } from "./userForm";


function MyForm(){
    const {inputValues,handleInputChange,resetForm} = UserForm({
        prenom:'',
        nom:'',
        email:'',
        phone:'',
        })
    const handleSubmit = (e) =>  {
        e.preventDefault()
        addUsers(inputValues)
        resetForm()
    }

   
    return(
        <form className="myForm" onSubmit={handleSubmit}>
            <h3>Jeemacoder Gestion d'utilisateurs</h3>
            <div className="d-flex ">
                <div className="form-group ml-3">
                <label htmlFor="prenom" className="form-label">Prenom</label>
                <input type='text'
                 id='prenom' 
                 name= 'prenom'
                 onChange={handleInputChange}
                 value={inputValues.prenom}
                  className='form-control'

                  />
                </div>   
                <div className="form-group ml-3">
                <label htmlFor="nom" className="form-label">Nom</label>
                <input type='text'
                 id='nom' 
                 name= 'nom' 
                 onChange={handleInputChange}
                 value={inputValues.nom}
                 className='form-control'
                 />
                </div>
            </div>  
           <div className="d-flex m-4">
                <div className="form-group mr-3">
                <label htmlFor="email" className="form-label">Email</label>
                <input type='email'
                    id='email'
                    name= 'email' 
                    onChange={handleInputChange}
                    value={inputValues.email}
                    className='form-control'
                  />
                </div>  
                <div className="form-group">
                    <label htmlFor="tel" className="form-label">Telephone</label>
                    <input type='tel'
                     id='tel'
                      name= 'phone' 
                      onChange={handleInputChange}
                      value={inputValues.phone}
                      className='form-control'
                      />
                </div>  
           </div> 
           <button className="btn btn-success mb-4" type="submit">Ajouter</button>    
        </form>
    )
}

export default MyForm