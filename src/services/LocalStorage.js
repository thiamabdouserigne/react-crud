import uuid from "react-uuid";


export const getUsersInfo = () => {
    if(!localStorage['@users']){
        localStorage['@users'] = JSON.stringify([])
    }
    let users = JSON.parse(localStorage['@users']);
    return users
}

export const addUsers = (user) => {
    const users = getUsersInfo();
    users.push({id: uuid(),...user});
    localStorage['@users'] = JSON.stringify(users)
}

export const deleteUser = (id) => {
    let users = getUsersInfo();
    users = users.filter((user) => user.id !== id);
    localStorage['@users'] = JSON.stringify(users);
}