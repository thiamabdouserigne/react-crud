import MyForm from './components/MyForm';
import Users from './components/Users';

function App() {
  return (
    <div className="container">
        <MyForm />
        <hr/>
        <Users />
    </div>
  );
}

export default App;
